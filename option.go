package gomcts

import (
	"math"
)

type Option struct {
	state                 State
	action                Action
	nextOptions           []*Option
	result                *Result // The result of this option, initially nil
	actionCount           int     // Number of actions taken until this option
	numPlayouts           int     // Total number of times this node run a simulation
	value                 int     // Number of wins and mnius number losses this node simulated
	ExplorationParameter  float64 // On choosing the next option to explore, how much to weigh in the number of explorations
	SecureChoiceParameter float64 // On choosing the final action to take, how much to weigh in the number of explorations
}

type State interface {
	Apply(Action) Outcome
	GetNextActions() []Action
	Clone() State
}

type Action interface {
	// Returns the index of the player that causes this action. This is required to keep track of who won
	GetActor() int
	IsEqual(Action) bool
}

func NewOption(state State, action Action, actionCount int) *Option {
	option := &Option{
		state:                 state.Clone(),
		action:                action,
		nextOptions:           nil,
		result:                nil,
		actionCount:           actionCount,
		numPlayouts:           0,
		value:                 0,
		ExplorationParameter:  1.44,
		SecureChoiceParameter: 1.0,
	}
	return option
}

func (option *Option) ExploreRoot(maxActionCount int, parallelExplorations int) {
	// This is slightly different to the normal explore function. This is meant to be the entry to the exploration
	// The difference to the normal explore is
	// - It does not check for final results (we expect always to explore at least one more step: `maxActionCount > 1`)
	// - It manages multiple threads by exploring the best N options (N=`parallelExploration`)
	option.numPlayouts += parallelExplorations
	option.applyAction()

	// Simulate and update this node's statistics
	resultsChannel := make(chan *Result, parallelExplorations)
	option.setNextOptions()

	// Start multiple explorations
	nextOptions := option.chooseOptions(parallelExplorations)
	for _, chosenOption := range nextOptions {
		go func(o *Option) {
			resultsChannel <- o.Explore(maxActionCount)
		}(chosenOption)
	}

	// Collect results
	for i := 0; i < len(nextOptions); i++ {
		result := <-resultsChannel
		option.updateNumWins(result)
	}
}

func (option *Option) Explore(maxActionCount int) *Result {
	// Main function of the recursive exploration
	option.numPlayouts += 1
	option.applyAction()
	if option.result.IsFinal() {
		option.updateNumWins(option.result)
		return option.result
	}

	if option.actionCount >= maxActionCount {
		// Simulation is stopped as it reached too many iterations
		// Do not store this result, as on the next exploration we might explore further
		result := NewResult(Tie, option.action)
		return result
	}

	// Simulate and update this node's statistics
	result := option.exploreNextOptions(maxActionCount)
	option.updateNumWins(result)
	return result
}

func (option *Option) GetNextOptionFromAction(action Action) *Option {
	for _, nextOption := range option.nextOptions {
		if nextOption.action.IsEqual(action) {
			return nextOption
		}
	}
	// This option never explored its next options or wasn't aware of this action due to pruning
	// e.g. there was a winning action possible, so the option only saw this, but the opponent didn't choose it
	newOption := NewOption(option.state, action, option.actionCount+1)
	newOption.applyAction()
	return newOption
}

func (option *Option) GetBestOptionToPlay() *Option {
	// Get option with highest secure child score
	bestOption := option.nextOptions[0]
	for _, nextOption := range option.nextOptions[1:] {
		if nextOption.getSecureChildScore() > bestOption.getSecureChildScore() {
			bestOption = nextOption
		}

	}
	return bestOption
}

func (option *Option) GetScoresNext() map[Action]float64 {
	scores := map[Action]float64{}
	for _, option := range option.nextOptions {
		scores[option.action] = option.getSecureChildScore()
	}
	return scores
}

func (option *Option) GetNumPlayoutsNext() map[Action]int {
	numPlayouts := map[Action]int{}
	for _, option := range option.nextOptions {
		numPlayouts[option.action] = option.numPlayouts
	}
	return numPlayouts
}

func (option *Option) GetWinningChance() float64 {
	return float64(option.value) / float64(option.numPlayouts)
}

func (option *Option) applyAction() {
	if option.result == nil {
		// We only run this once and use the cached values in the future
		outcome := option.state.Apply(option.action)
		option.result = NewResult(outcome, option.action)
	}
}

func (option *Option) exploreNextOptions(maxActionCount int) *Result {
	option.setNextOptions()
	chosenOption := option.chooseOptions(1)[0]
	return chosenOption.Explore(maxActionCount)
}

func (option *Option) setNextOptions() {
	if option.nextOptions == nil {
		option.nextOptions = []*Option{}
		for _, action := range option.state.GetNextActions() {
			nextOption := NewOption(option.state, action, option.actionCount+1)
			option.nextOptions = append(option.nextOptions, nextOption)
		}
	}
}

func (option *Option) chooseOptions(num int) []*Option {
	// Safety guard to not return empty options, if more options are requested than exist
	if len(option.nextOptions) < num {
		num = len(option.nextOptions)
	}
	// Get options with highest score
	bestOptions := make([]*Option, num)
	bestScores := make([]float64, num)
	for i := range bestScores {
		bestScores[i] = math.Inf(-1)
	}

	for _, nextOption := range option.nextOptions {
		nextScore := nextOption.getScore(option.numPlayouts)
		for i := len(bestScores) - 1; i >= 0; i-- {
			if nextScore > bestScores[i] {
				// Found a higher score
				for j := 0; j < i; j++ {
					// Shift every old entry one down
					bestScores[j] = bestScores[j+1]
					bestOptions[j] = bestOptions[j+1]
				}
				// Set new entry and finish rearranging
				bestScores[i] = nextScore
				bestOptions[i] = nextOption
				break
			}
		}
	}
	return bestOptions
}

func (option *Option) updateNumWins(result *Result) {
	if result.DidActorWin(option.action.GetActor()) {
		option.value += 1
	}
	if result.DidActorLoose(option.action.GetActor()) {
		option.value -= 1
	}
}

func (option *Option) getScore(parentNumPlayouts int) float64 {
	// General score function for selecting the next action to explore
	exploitation := float64(option.value) / float64(option.numPlayouts+1)
	exploration := math.Sqrt(math.Log(float64(parentNumPlayouts+1)) / (float64(option.numPlayouts + 1)))
	return exploitation + option.ExplorationParameter*exploration
}

func (option *Option) getSecureChildScore() float64 {
	// Score function for final select of action to play
	// From "Winands, M.H., Björnsson, Y. and Saito, J.T., 2008. Monte-Carlo tree search solver"
	exploitation := float64(option.value) / float64(option.numPlayouts+1)
	exploration := option.SecureChoiceParameter / math.Log(float64(option.numPlayouts+1))
	return exploitation + exploration
}
