package gomcts

// Action is what caused the result
type Result struct {
	outcome    Outcome
	finalActor int
}

type Outcome int

// NotFinal if the current state of the game is not a final state yet
const (
	NotFinal Outcome = iota
	Win
	Tie
)

func NewResult(outcome Outcome, action Action) *Result {
	return &Result{
		outcome:    outcome,
		finalActor: action.GetActor(),
	}
}

func NewNotFinalResult() *Result {
	// Required only for initial root option, as the initial node has no result but also no action yet
	return &Result{
		outcome:    NotFinal,
		finalActor: 0,
	}
}

func (result *Result) IsFinal() bool {
	return result.outcome != NotFinal
}

func (result *Result) DidActorWin(actor int) bool {
	if result.outcome == Win {
		return result.finalActor == actor
	}
	return false
}

func (result *Result) DidActorLoose(actor int) bool {
	if result.outcome == Win {
		return result.finalActor != actor
	}
	return false
}
