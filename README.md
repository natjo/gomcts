# Go MCTS

Golang Module to run Monte Carlo Tree Search. Allows multithreading to find results even faster!
Also allows multi-agent search for scenarios with more than two actors.

## How to use this
All interaction with the tree search algorithm is done via the `Tree` type as explained in the next chapter. To make the search work, and to inialize the tree properly, you have to define the two types `State` and `Action` as described further below.

### Tree
Type that controls the start of the search and provides functionality to easily interact with the tree search and retrieve (intermediary) results.

You start the search by initializing and using this type.
```go
func NewTree(state State, searchDepth int, startingPlayer int, explorationTime time.Duration) *Tree
```

You can further configure the search by calling these functions on the `Tree`.
```go
func (tree *Tree) SetThreads(threads int)
func (tree *Tree) SetExplorationParameter(explorationParameter float64)
func (tree *Tree) SetPrintLogs(printLogs bool)
```

Once configured, you can trigger a run to find the next optimal action with
```go
func (tree *Tree) StartExplore()
```
And once it finished retrieve the best found option with
```go
func (tree *Tree) GetBestAction() Action
```
Finally, to advance the tree, you have to run 
```go
func (tree *Tree) UpdateFromAction(action Action)
```

You have to define two types defining the following interfaces to allow the search to correctly function. See the next two chapters.

### State
Contains all information about the current state of a game.
```go
type State interface {
    // Returns the Outcome after the Action leading to this State is applied
    // See more about Outcome below 
    Apply(Action) Outcome
    // Returns a list of possible next actions
    GetNextActions() []Action
    // Returns a deep clone of this State
    Clone() State
}
```

### Action
Contains information to advance a `State` in the game by one step.
```go
type Action interface {
    // Returns the index of the player that causes this action. This is required to keep track of who won
    GetActor() int
    // Comparison between Actions
    IsEqual(Action) bool
}
```

A `Action` is returned from the `Tree` type to indicate the optimal action to take. Therefor it makes sense to add further functionality to this type, that allows you to retrieve information about the action. This is not required to make the search work though.

### Result
Shows the current outcome of the game. Final actor is required to allow the search algorithm to backtrack for the correct player at each step.
```go
type Result struct {
    outcome    Outcome
    finalActor int
}
```

`outcome` must be one of the predefined constants `NotFinal`, `Win` or `Tie`.