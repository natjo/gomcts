package gomcts

import (
	"log"
	"math"
	"time"
)

type Tree struct {
	rootOption      *Option
	searchDepth     int
	explorationTime time.Duration
	threads         int
	printLogs       bool
	stopOnDiff      float64
}

func NewTree(state State, searchDepth int, startingPlayer int, explorationTime time.Duration) *Tree {
	rootOption := NewOption(state, &DummyAction{player: startingPlayer}, 0)
	// The initial result can safely be set to NotFinal
	rootOption.result = NewNotFinalResult()
	return &Tree{
		rootOption:      rootOption,
		searchDepth:     searchDepth,
		explorationTime: explorationTime,
		threads:         1,
		printLogs:       false,
		stopOnDiff:      0.001,
	}
}

// The default number of threads is 1
func (tree *Tree) SetThreads(threads int) {
	tree.threads = threads
}

// Early stop the search if the change of score between the best action is less than this value
func (tree *Tree) SetStopOnDiff(stopOnDiff float64) {
	tree.stopOnDiff = stopOnDiff
}

// The default exploration parameter is 1.44
func (tree *Tree) SetExplorationParameter(explorationParameter float64) {
	tree.rootOption.ExplorationParameter = explorationParameter
}

// If this is set to true, the search will automatically log results regularly
func (tree *Tree) SetPrintLogs(printLogs bool) {
	tree.printLogs = printLogs
}

func (tree *Tree) StartExplore() {
	maxActionCount := tree.rootOption.actionCount + tree.searchDepth
	tree.log("Explore until depth %v", maxActionCount)

	timesLogged := 0
	lastBestActionWinningChance := 0.0
	for start := time.Now(); time.Since(start) < tree.explorationTime; {
		if time.Since(start) > time.Millisecond*500*time.Duration(timesLogged+1) {
			timesLogged += 1

			// Check the current best option, log and stop early if change of winning chance did not change enough
			bestOption := tree.rootOption.GetBestOptionToPlay()
			bestActionWinningChance := bestOption.GetWinningChance()
			tree.log("Best option found after %v explorations, winning chance: %f", tree.rootOption.numPlayouts, bestOption.GetWinningChance())
			if math.Abs(bestActionWinningChance-lastBestActionWinningChance) < tree.stopOnDiff {
				break
			}
		}
		tree.rootOption.ExploreRoot(maxActionCount, tree.threads)
	}
}

func (tree *Tree) GetBestAction() Action {
	bestOption := tree.rootOption.GetBestOptionToPlay()
	tree.log("Best option found after %v explorations, winning chance: %f", tree.rootOption.numPlayouts, bestOption.GetWinningChance())
	return bestOption.action
}

func (tree *Tree) UpdateFromAction(action Action) {
	tree.rootOption = tree.rootOption.GetNextOptionFromAction(action)
}

// Returns the scores of each next possible action
func (tree *Tree) GetScoresNext() map[Action]float64 {
	return tree.rootOption.GetScoresNext()
}

// Returns the number of times each action was played out
func (tree *Tree) GetNumPlayoutsNext() map[Action]int {
	return tree.rootOption.GetNumPlayoutsNext()
}

func (tree *Tree) log(format string, v ...any) {
	if tree.printLogs {
		log.Printf(format, v...)
	}
}

// Required in case the bot starts the game
// Since the first option has no action (it's the start of the game)
type DummyAction struct {
	player int
}

func (a *DummyAction) GetActor() int {
	return a.player
}

func (a *DummyAction) IsEqual(Action) bool {
	return false
}
